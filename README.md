# Overview
Simple PKI, written in BASH using OpenSSL binary. PKISS stands for "PKI Keep it Simple Stupid!" and provides the basic functionality to manage certificates:
- few signing CAs under the same Root CA
- grant cerificate(s) with or without CRL [in batch mode]
- revoke cerificate(s) [in batch mode]
- list / check granted certificates


https://gitlab.com/doka.ua/pkiss


While can be easily expanded, it suports (by default) generation of server and client certificates:

- client is:
  - keyUsage         = critical, nonRepudiation, digitalSignature, keyEncipherment
  - extendedKeyUsage = clientAuth
- server is:
  - keyUsage         = critical, nonRepudiation, digitalSignature, keyEncipherment, keyAgreement
  - extendedKeyUsage = clientAuth, serverAuth

which covers most of the scenarios. Additional kinds of certificates can be easily created by adding respective 'kind.conf' to the etc/ and respective changes to the etc/signing-ca.conf

PKISS operates with the following chain: Root -> Signing -> endCert. Initialization consists of at least two steps:
- initialize Root CA
- initialize Signing CA
  - note: you can generate plenty of Sigining CAs using the same Root CA
- start generate cerificates using initialized Signing CA(s)

# License

It is prohibited to use PKISS:
- on territories of Russian Federation and it's allies (namely - Belarus, Iran, North Korea), occupied by Russian Federation or under control of Russian Federation;
- in interests of Russian Federation and it's allies (namely - Belarus, Iran, North Korea);
- in interests of companies that are on the official sanction lists of Ukraine or Great Britain or the United States of America or Canada or the European Union or members of the European Union.

There are no other limitations on any usage of this tool.

# Install
Clone the repository to the server, where Root Certificate Authority will reside.

## Init ROOT CA
```
$ ./init_root.sh -h

Usage:

./init_root.sh \ 
  -p|--path WHERE_TO_INSTALL \ 
  -n|--name "CA NAME" \ 
  -c|--country "COUNTRY" \ 
  -o|--org "ORG NAME" \ 
  -u|--unit "ORG UNIT" \ 
  -e|--email "email" \ 
  --use-crl true|false \ 
  --base-url CRL_BASE_URL \ 
  -b|--bits RSA_KEY_SIZE \ 
  -m|--md MESSAGE_DIGEST

RSA Key size default is 2048 bits
Message Digest default is sha256

e.g. ./init_root.sh -p /opt/rootca \ 
        -n R1 -c UA -o MyOrg -u "MyOrg SOC" -e soc@myorg.net.xyz \ 
        --use-crl true --base-url http://myorg.net.xyz/crl
```

While everything is self-explainable, the note on CRL URL generation:

- if you will choose to add CRL information to the certificates
- the full URL will be a combination of Base URL, Org Name and CA Name
- where all spaces in names will be changed to [the single] underscore
- e.g. resulting URL http://myorg.net.xyz/crl/My_Fun_Org-R_1.crl will be build upon these parameters
  - Base URL: `http://myorg.net.xyz/crl`
  - Org Name: `My  Fun  Org`
  - CA Name: `R 1`

## Generate Signing CA
Go to the directory with just generated Root CA.

```
$ ./init_signing.sh -h

Usage:

./init_signing.sh \ 
  -n|--name "CA NAME" \ 
  -u|--unit "ORG UNIT" \ 
  -e|--email "email" \ 
  --use-crl true|false \ 
  --base-url CRL_BASE_URL \ 
  -b|--bits RSA_KEY_SIZE \ 
  -m|--md MESSAGE_DIGEST \
  -t|--signing-template template_name

RSA Key size default is 2048 bits
Message Digest default is sha256
--signing-template will look at ./src/etc/template_name.conf.template (default is signing-ca)

e.g. ./init_signing.sh \ 
        -n A1 -u "MyOrg SOC" -e soc@myorg.net.xyz \ 
        --use-crl true --base-url http://myorg.net.xyz/crl
```

Note not the same amount of parameters when initializing Signing CA. They will be read from etc/pkiss.conf, which is generated on the previous step and aren't subject to change (while you can try, why not?!)

After completition, the following message will appear:
```
*****************************************************************
  signing_A1.tar.gz is ready to be copied to another server/place.
  Unpack it and use signing_A1/pkiss to generate certificates
*****************************************************************
```

You can copy the archive to another server (thus keeping your Root CA hidden), unpack it and start using PKISS.
You can keep archive of the Signing CA on the Root CA, since this can be your emergency original copy of Signing CA.

# Use

PKISS use configuration files, which are located under the directory where it launched from. For example, if pkiss located in `/opt/signing-A1/` directory, then regardless of whether you will use `./pkiss ...` or `/opt/signing-A1/pkiss ...` or `./signing-A1/pkiss`, the directory `/opt/signing-A1/etc` will be used as the source for PKISS and openssl operations.

## Signing CA directory structure
- CA root directory contains pkiss and pkiss-publish.sh executables
- etc/ contains openssl and PKISS configuration files
- ca/ contains certificates and keys for Root and Signing CAs
- crl/ contains CRL files
- certs/ contains certificates and keys, created by PKISS operations
    - certs/arc/ contains archived certificates and keys (see below)
    - certs/revoked/ contains revoked certificates and keys

**Note:** every active certificate (certs/ and certs/arc/) consists of three files:
- ID.crt - certificate itself
- ID.key - private key
- ID.fullchain.crt - combination of ca/ca-chain.crt, ID.crt and ID.key, done for convenience of import - this is full chain of trust for the certificate.

**Note:** pkiss-publish.sh used by pkiss to publish Certificate Revocation Lists. Update it according to your environment.

## Create certificates
There can be just two certificates for single entity (server or client) - one is active, another one is archived. There is no difference between them - both versions are fully operable and can be used simultaneously. When you create certificate and it already exists - you will be prompted to archive existing. Archived certificates will be moved to certs/arc/ directory and listed separately. When you create certificate and both active and archive versions exist, you will be advised to revoke one of these versions.

- pkiss [-d days] [--use-crl true|false] [--base-url URL] server|client [template] CN [CN1 [CN2 [ ... ]]]
    - create server or client certificate for the specified CN(s)
      - optionally (if specified), pkiss will use etc/template.conf (if exists) and template_ext* section in etc/signing-ca.conf for this procedure
      - for 'server', CN must be either FQDN or IPv4/IPv6 address
    - days is optional argument, specify validity for certificate (default is 365 days)
    - --use-crl and --base-url are optional arguments, which override configured in etc/pkiss.conf

## Revoke certificates
- pkiss revoke CN [CN [CN [ ... ]]]
    - revoke certificate for the specified CN
        - to revoke archived certificate, use prefix arc/CN
    - publish updated CRL using pkiss-publish.sh
- pkiss revokearc
    - revoke ALL archived certificates
    - publish updated CRL using pkiss-publish.sh

## Other
- pkiss list[/t]
    - print the list of active certificates, sorted by alphabet
    - /t changes sort order to date (newest first)
- pkiss gencrl
    - generate CRL list
    - publish CRL list using pkiss-publish.sh
- pkiss check[/t]
    - check and report status of certificates
    - /t changes sort order to date (newest first)
    - if running in terminal session - print report on the screen
    - if running in daemon session (e.g. from crontab) - email report
- pkiss email CN e@mail
    - insecurely send specified full chain to the specified recipient

## OpenSSL tips and tricks

### View all certificates in the bundle
while openssl x509 -noout -text; do : ; done < fullchain.pem

# Used materials
- https://pki-tutorial.readthedocs.io/en/latest/simple/
- https://jamielinux.com/docs/openssl-certificate-authority/introduction.html
- https://www.phildev.net/ssl/opensslconf.html
- https://superuser.com/questions/738612/openssl-ca-keyusage-extension
- https://www.feistyduck.com/library/openssl-cookbook/online/

# Author info
Volodymyr Litovka

doka@funlab.cc

https://www.linkedin.com/in/vlitovka/

