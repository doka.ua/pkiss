#!/bin/bash

params_mandatory="TARGET COUNTRY CA_NAME ORG_NAME ORG_UNIT EMAIL USE_CRL"
params_optional="BASE_URL BITS MD"

APP_DIR=$(dirname $(realpath $0))
err=false

cleanup_and_error() {
  # Double-check that $TARGET is not '/'
  [ "$TARGET" == "/" ] || rm -rfv "$TARGET"
  exit 1
}

help() {
  echo
  echo "Usage:"
  echo
  echo "$0 \ "
  echo "  -p|--path WHERE_TO_INSTALL \ "
  echo "  -n|--name \"CA NAME\" \ "
  echo "  -c|--country \"COUNTRY\" \ "
  echo "  -o|--org \"ORG NAME\" \ "
  echo "  -u|--unit \"ORG UNIT\" \ "
  echo "  -e|--email \"email\" \ "
  echo "  --use-crl true|false \ "
  echo "  --base-url CRL_BASE_URL \ "
  echo "  -b|--bits RSA_KEY_SIZE \ "
  echo "  -m|--md MESSAGE_DIGEST"
  echo
  echo "RSA Key size default is 2048 bits"
  echo "Message Digest default is sha256"
  echo
  echo "e.g. $0 -p /opt/myorg_root \ "
  echo "        -n R1 -c UA -o MyOrg -u \"MyOrg SOC\" -e soc@myorg.net.xyz \ "
  echo "        --use-crl true --base-url http://myorg.net.xyz/crl"
  echo
}

while [ -n "$1" ]; do
  case $1 in
    -p|--path)
      if [ "$2" == "/" ]; then
        echo "ERROR: CA directory cannot be '$2'"
      elif [ -e "$2" ]; then
        echo "ERROR: '$2' already exists"
      else
        # Remove trailing '/'
        TARGET=$(echo $2 | sed -E 's/\/+$//g')
      fi
      ;;
    -n|--name)
      CA_NAME=$2 ;;
    -c|--country)
      COUNTRY=$2 ;;
    -o|--org)
      ORG_NAME=$2 ;;
    -u|--unit)
      ORG_UNIT=$2 ;;
    -e|--email)
      EMAIL=$2 ;;
    --use-crl)
      case $2 in
        true|false)
          USE_CRL=$2
          $USE_CRL && crlext="" || crlext="_nocrl" ;;
        *)
          help; exit
      esac
      ;;
    --base-url)
      # Remove trailing '/'
      BASE_URL=$(echo $2 | sed -E 's/\/+$//g') ;;
    -b|--bits)
      BITS=$2 ;;
    -m|--md)
      MD=$2 ;;
    *)
      help
      exit 1
  esac
  shift
  shift
done

for parm in $params_mandatory; do
  if [ -z "${!parm}" ]; then
    echo "$parm not set, cannot continue ..."
    err=true
  fi
done

if $USE_CRL && [ -z "$BASE_URL" ]; then
  echo
  echo "ERROR: --use-crl set to true, but --base-url is not set"
  err=true
fi

BITS=${BITS:-2048}
MD=${MD:-sha256}

if ! [[ $BITS =~ ^[0-9]+$ ]]; then
  echo "RSA Key size must be a numerical value"
  err=true
fi

$err && { help; exit 1; }

echo "Will init new ROOT CA using these parameters:"
for parm in $params_mandatory $params_optional; do
  printf " - %10s : '%s'\n" "$parm" "${!parm}"
done
echo
read -p "Check carefully and either press Enter to continue or Ctrl-C to stop" yn

# These are parts of filename, remove spaces for convenience
F_ORG_NAME=$(echo $ORG_NAME | sed -E 's/[ \t]+/_/g')
F_CA_NAME=$(echo $CA_NAME | sed -E 's/[ \t]+/_/g')

# ----- Copy required files
mkdir -p $TARGET/etc || cleanup_and_error

# Save parameters
for parm in $params_mandatory $params_optional; do
  printf '%s="%s"\n' "$parm" "${!parm}" >> $TARGET/etc/pkiss.conf
done

for template in $APP_DIR/root-ca.conf.template $APP_DIR/src/pkiss-publish.sh.template ; do
  # Remove trailing 'template*'
  ef=$(basename $template | rev | cut -d. -f2- | rev)
  cp $template $TARGET/etc/$ef || cleanup_and_error
  # Configured and calculated parameters
  for parm in $params_optional $params_mandatory F_ORG_NAME F_CA_NAME; do
    sed -Ei "s|%${parm}%|${!parm}|g" $TARGET/etc/$ef
  done
done
# This was copied to etc/ above, move it to the home
mv $TARGET/etc/pkiss-publish.sh $TARGET && chmod a+x $TARGET/pkiss-publish.sh
# Copy sources for Signing CA
cp -R $APP_DIR/src $TARGET
mv $TARGET/src/init_signing.sh $TARGET && chmod a+x $TARGET/init_signing.sh

# --- Let's go with certificate itlsef
echo
while [ -z "$CA_PASS" ]; do
  read -rs -p "Provide ROOT CA key password: " CA_PASS
  echo
done
CA_PASS_VRFY=$RANDOM
while [ "$CA_PASS" != "$CA_PASS_VRFY" ]; do
  read -rs -p "Verify ROOT CA key password: " CA_PASS_VRFY
  echo
done
echo
export CA_PASS

mkdir -p $TARGET/ca/root-ca/private $TARGET/ca/root-ca/db $TARGET/crl
chmod 700 $TARGET/ca/root-ca/private
#- Database
cp /dev/null $TARGET/ca/root-ca/db/root-ca.db
# https://serverfault.com/a/859051/537876
# Usa parameter from config file
cat $TARGET/etc/root-ca.conf |
  grep ^unique_subject |
  sed -E 's/\s+/ /g; s/#.*//g; s/\s*$//g' > $TARGET/ca/root-ca/db/root-ca.db.attr
echo 01 > $TARGET/ca/root-ca/db/root-ca.crt.srl
echo 01 > $TARGET/ca/root-ca/db/root-ca.crl.srl
#- Create Root CA
openssl genrsa -aes256 -out $TARGET/ca/root-ca/private/root-ca.key -passout env:CA_PASS $BITS
openssl req -new \
    -config $TARGET/etc/root-ca.conf \
    -out $TARGET/ca/root-ca.csr \
    -key $TARGET/ca/root-ca/private/root-ca.key -passin 'env:CA_PASS' || cleanup_and_error
openssl ca -selfsign -passin 'env:CA_PASS' -batch \
    -config $TARGET/etc/root-ca.conf \
    -in $TARGET/ca/root-ca.csr \
    -extensions root_ca_ext$crlext | openssl x509 -subject -enddate -serial -out $TARGET/ca/root-ca.crt || cleanup_and_error
rm -f $TARGET/ca/root-ca.csr

if $USE_CRL ; then
  echo "======= GENERATE CRL ======="
  openssl ca -config $TARGET/etc/root-ca.conf -passin 'env:CA_PASS' \
    -gencrl -out $TARGET/crl/${F_ORG_NAME}-${F_CA_NAME}.pem || cleanup_and_error
  openssl crl -in $TARGET/crl/${F_ORG_NAME}-${F_CA_NAME}.pem \
    -outform DER -out $TARGET/crl/${F_ORG_NAME}-${F_CA_NAME}.crl || cleanup_and_error
  #
  unset CA_PASS
  if [ -x "$TARGET/pkiss-publish.sh" ] && $TARGET/pkiss-publish.sh ; then
    echo "CRL published successfully"
  else
    echo "Was unable to publish CRL, verify $TARGET/pkiss-publish.sh"
  fi
fi

