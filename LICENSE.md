https://gitlab.com/doka.ua/pkiss

# License

It is prohibited to use PKISS:
- on territories of Russian Federation and it's allies (namely - Belarus, Iran, North Korea), occupied by Russian Federation or under control of Russian Federation;
- in interests of Russian Federation and it's allies (namely - Belarus, Iran, North Korea);
- in interests of companies that are on the official sanction lists of Ukraine or Great Britain or the United States of America or Canada or the European Union or members of the European Union.

There are no other limitations on any usage of this tool.

# Author info
Volodymyr Litovka

doka@funlab.cc

https://www.linkedin.com/in/vlitovka/
