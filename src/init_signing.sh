#!/bin/bash

params_mandatory="COUNTRY CA_NAME ORG_NAME ORG_UNIT EMAIL USE_CRL"
params_optional="BASE_URL BITS MD SIGNING_TEMPLATE"

APP_DIR=$(dirname $(realpath $0))
err=false

# Read configuration and unset CA_NAME
source $APP_DIR/etc/pkiss.conf
unset CA_NAME

cleanup_and_error() {
  # Double-check that $TARGET is not '/'
  [ "$TARGET" == "/" ] || rm -rfv "$TARGET"
  exit 1
}

help() {
  echo
  echo "Usage:"
  echo
  echo "$0 \ "
  echo "  -n|--name \"CA NAME\" \ "
  echo "  -u|--unit \"ORG UNIT\" \ "
  echo "  -e|--email \"email\" \ "
  echo "  --use-crl true|false \ "
  echo "  --base-url CRL_BASE_URL \ "
  echo "  -b|--bits RSA_KEY_SIZE \ "
  echo "  -m|--md MESSAGE_DIGEST \ "
  echo "  -t|--signing-template template_name"
  echo
  echo "RSA Key size default is 2048 bits"
  echo "Message Digest default is sha256"
  echo "--signing-template will look at $APP_DIR/src/etc/template_name.conf.template (default is signing-ca)"
  echo
  echo "e.g. $0 \ "
  echo "        -n A1 -u \"MyOrg SOC\" -e soc@myorg.net.xyz \ "
  echo "        --use-crl true --base-url http://myorg.net.xyz/crl"
  echo
}

while [ -n "$1" ]; do
  case $1 in
    -n|--name)
      CA_NAME=$2 ;;
    -u|--unit)
      ORG_UNIT=$2 ;;
    -e|--email)
      EMAIL=$2 ;;
    --use-crl)
      case $2 in
        true|false)
          USE_CRL=$2 ;;
        *)
          echo "Invalid --use-crl value"
          err=true
      esac
      ;;
    --base-url)
      # Remove trailing '/'
      BASE_URL=$(echo $2 | sed -E 's/\/+$//g') ;;
    -b|--bits)
      BITS=$2 ;;
    -m|--md)
      MD=$2 ;;
    -t|--signing-template)
      SIGNING_TEMPLATE=${2:-"signing-ca"}
      if ! [ -f "$APP_DIR/src/etc/$SIGNING_TEMPLATE.conf.template" ]; then
        echo "$APP_DIR/src/etc/$SIGNING_TEMPLATE.conf.template is not present"
        err=true
      fi
      ;;
    *)
      err=true ;;
  esac
  shift
  shift
done

for parm in $params_mandatory; do
  if [ -z "${!parm}" ]; then
    echo "$parm not set, cannot continue ..."
    err=true
  fi
done

if $USE_CRL && [ -z "$BASE_URL" ]; then
  echo
  echo "ERROR: --use-crl set to true, but --base-url is not set"
  err=true
fi

BITS=${BITS:-2048}
MD=${MD:-sha256}

if ! [[ $BITS =~ ^[0-9]+$ ]]; then
  echo "RSA Key size must be a numerical value"
  err=true
fi

$err && { help; exit 1; }

$USE_CRL && crlext="" || crlext="_nocrl"
# These are parts of filename, remove spaces for convenience
F_ORG_NAME=$(echo $ORG_NAME | sed -E 's/[ \t]+/_/g')
F_CA_NAME=$(echo $CA_NAME | sed -E 's/[ \t]+/_/g')

TARGET="$APP_DIR/signing_$F_CA_NAME"
if [ -d "$TARGET" ] || [ -f "$TARGET.tar.gz" ]; then
  echo "Signing CA '$CA_NAME' already exists"
  exit 1
fi

echo "Will init new SIGNING CA using these parameters:"
for parm in $params_mandatory $params_optional; do
  printf " - %16s : '%s'\n" "$parm" "${!parm}"
done
echo
read -p "Check carefully and either press Enter to continue or Ctrl-C to stop" yn

# ----- Copy required files
mkdir -p $TARGET/etc || cleanup_and_error

# Save init parameters
echo "# These are parameters just for PKISS" > $TARGET/etc/pkiss.conf
echo "# OpenSSL parameters are in its respective config files" >> $TARGET/etc/pkiss.conf
for parm in $params_mandatory $params_optional; do
  printf '\n%s="%s"\n' "$parm" "${!parm}" >> $TARGET/etc/pkiss.conf
done

for template in $APP_DIR/src/etc/*.template ; do
  # Remove trailing 'template*'
  ef=$(basename $template | rev | cut -d. -f2- | rev)
  cp $template $TARGET/etc/$ef || cleanup_and_error
  for parm in $params_optional $params_mandatory F_ORG_NAME F_CA_NAME; do
    sed -Ei "s|%${parm}%|${!parm}|g" $TARGET/etc/$ef
  done
done

# PKISS operate using signing-ca.conf so renaming specified template
mv -f $TARGET/etc/$SIGNING_TEMPLATE.conf $TARGET/etc/signing-ca.conf 2>/dev/null
# Copy pkiss
cp $APP_DIR/src/pkiss $TARGET && chmod a+x $TARGET/pkiss
# Special treatment for pkiss-publish.sh
cp $APP_DIR/src/pkiss-publish.sh.template $TARGET/pkiss-publish.sh && chmod a+x $TARGET/pkiss-publish.sh
sed -Ei 's|%TARGET%/||g' $TARGET/pkiss-publish.sh

# --- Let's go with certificate itlsef
echo
while [ -z "$CA_PASS" ]; do
  read -rs -p "Provide Signing CA key password: " CA_PASS
  echo
done
CA_PASS_VRFY=$RANDOM
while [ "$CA_PASS" != "$CA_PASS_VRFY" ]; do
  read -rs -p "Verify Signing CA key password: " CA_PASS_VRFY
  echo
done
echo
export CA_PASS

echo
while [ -z "$ROOT_PASS" ]; do
  read -rs -p "Enter Root CA key password: " ROOT_PASS
  echo
done
export ROOT_PASS

cd $TARGET
mkdir -p $TARGET/ca/signing-ca/private $TARGET/ca/signing-ca/db $TARGET/crl $TARGET/certs
chmod 700 $TARGET/ca/signing-ca/private
#- Database
cp /dev/null $TARGET/ca/signing-ca/db/signing-ca.db
# https://serverfault.com/a/859051/537876
# Usa parameter from config file
cat $TARGET/etc/signing-ca.conf |
  grep ^unique_subject |
  sed -E 's/\s+/ /g; s/#.*//g; s/\s*$//g' > $TARGET/ca/signing-ca/db/signing-ca.db.attr
echo 01 > $TARGET/ca/signing-ca/db/signing-ca.crt.srl
echo 01 > $TARGET/ca/signing-ca/db/signing-ca.crl.srl
#- Create Signing CA
openssl genrsa -aes256 -out $TARGET/ca/signing-ca/private/signing-ca.key -passout env:CA_PASS $BITS
openssl req -new \
    -config $TARGET/etc/signing-ca.conf \
    -out $TARGET/ca/signing-ca.csr \
    -key $TARGET/ca/signing-ca/private/signing-ca.key -passin 'env:CA_PASS' || cleanup_and_error
openssl ca -passin 'env:ROOT_PASS' -batch \
    -config $APP_DIR/etc/root-ca.conf \
    -in $TARGET/ca/signing-ca.csr \
    -extensions signing_ca_ext$crlext | openssl x509 -subject -enddate -serial -out $TARGET/ca/signing-ca.crt || cleanup_and_error
rm -f $TARGET/ca/signing-ca.csr

if $USE_CRL ; then
  echo "======= GENERATE CRL ======="
  openssl ca -config $TARGET/etc/signing-ca.conf -passin 'env:CA_PASS' \
    -gencrl -out $TARGET/crl/${F_ORG_NAME}-${F_CA_NAME}.pem || cleanup_and_error
  openssl crl -in $TARGET/crl/${F_ORG_NAME}-${F_CA_NAME}.pem \
    -outform DER -out $TARGET/crl/${F_ORG_NAME}-${F_CA_NAME}.crl || cleanup_and_error
  #
  unset CA_PASS
  unset ROOT_PASS
  if [ -x "$TARGET/pkiss-publish.sh" ] && $TARGET/pkiss-publish.sh ; then
    echo "CRL published successfully"
  else
    echo "Was unable to publish CRL, verify $TARGET/pkiss-publish.sh"
  fi
fi

# Copy root-ca public cert and create fullchain in signing environment
cp $APP_DIR/ca/root-ca.crt $TARGET/ca
cat $TARGET/ca/signing-ca.crt $TARGET/ca/root-ca.crt > $TARGET/ca/ca-chain.crt

# Create ready-to-copy archive and delete directory
cd $APP_DIR
ARCNAME=$(basename $TARGET)
tar czf $ARCNAME.tar.gz $ARCNAME
rm -rf $TARGET

echo
echo "*****************************************************************"
echo "  $ARCNAME.tar.gz is ready to be copied to another server/place."
echo "  Unpack it and use $ARCNAME/pkiss to generate certificates"
echo "*****************************************************************"
